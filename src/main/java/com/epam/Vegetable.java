package com.epam;

public abstract class Vegetable  {
    public int calories;
    public int weight;
    public String name;
    @Override
    public String toString(){
        return this.name+" "+this.calories;
    }
}
