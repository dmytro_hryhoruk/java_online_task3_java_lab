package com.epam;

import com.epam.Vegetables.Fruits;
import com.epam.Vegetables.Roots;
import com.epam.Vegetables.Seeds;

public class ChefCook {
    public static void main(String[] args) {
        Roots carrot = new Roots("Carrot", 232);
        Seeds peper = new Seeds("Black peper", 27);
        Fruits tomato = new Fruits("Rose tomato", 67);
        Fruits cucumber = new Fruits("Green cucumber", 58);
        Salads coreanCarrot = Salads.KOREAN_CARROT;
        Salads tomatoAndCucumber = Salads.TOMATO_AND_CUCUMBER;
        tomatoAndCucumber.salad.add(tomato);
        tomatoAndCucumber.salad.add(cucumber);
        coreanCarrot.salad.add(peper);
        coreanCarrot.salad.add(carrot);
        coreanCarrot.printIngridientsOfSalad(coreanCarrot.salad);
        coreanCarrot.printTotalOfSaladsCalories(coreanCarrot.salad);
        tomatoAndCucumber.printIngridientsOfSalad(tomatoAndCucumber.salad);
        tomatoAndCucumber.printTotalOfSaladsCalories(tomatoAndCucumber.salad);
      
    }
}
