package com.epam;
import java.util.ArrayList;

public enum Salads {

    KOREAN_CARROT(2),
    TOMATO_AND_CUCUMBER(2);

    public int numberOfIngridients;
    ArrayList<Vegetable> salad;

    Salads(int numberOfIngridients) {
        this.numberOfIngridients = numberOfIngridients;
        salad = new ArrayList<Vegetable>();
    }
    public void printIngridientsOfSalad(ArrayList<Vegetable> salad) {
        System.out.println(Salads.this + " has " + Salads.this.numberOfIngridients + " ingridients");
        for (int i = 0; i < salad.size(); i++)
            System.out.println((i + 1) + ". " + salad.get(i).name);
    }
    public void printTotalOfSaladsCalories(ArrayList<Vegetable> salad) {
        int sumOfCalories = 0;
        for (int i = 0; i < salad.size(); i++) {
            sumOfCalories += salad.get(i).calories;
        }
        System.out.println(Salads.this + " has " + sumOfCalories + " calories");
    }


}
